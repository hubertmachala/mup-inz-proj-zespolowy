<?php
$json = file_get_contents($API.'/eksponaty/'.$_GET['eksponat']);
$eksponaty = json_decode($json, true);
?>

<div class="container">
<div class="row justify-content-center">
  <div class="col-lg-8">
<div class="jumbotron jumbotron-fluid" style="margin: 50px 0;">
  <div class="container">
    <h1 class="display-4">Witamy na stronie Muzeum</h1>
    <h2>Podstrona eksponatu o ID = <?php echo $eksponaty['id']; ?></h2>
    <p class="lead"><?php echo $eksponaty['tytul']; ?></p>
  </div>
</div>
</div>
</div>


<div class="container" style="margin-bottom: 25px;">
<div class="row justify-content-center">
  <div class="col-lg-8">
    <div class="card">
  <img class="card-img-top" src="<?php echo $eksponaty['url_grafika']; ?>" alt="<?php echo $eksponaty['tytul']; ?>">
  <div class="card-body">
    <h5 class="card-title"><?php echo $eksponaty['tytul']; ?></h5>
    <p class="card-text"><?php echo $eksponaty['opis']; ?></p>
  </div>
</div>
</div>
</div>