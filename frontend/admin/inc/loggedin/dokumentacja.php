<h1>Endpointy API</h1>
<p>[POST] http://machala.net.pl/mup-project/api/public/auth (przyjmuje login i haslo) (zwraca obiekt z tokenem lub obiekt z błędem</p>
<p>[GET] http://machala.net.pl/mup-project/api/public/eksponaty (nie przyjmuje wartosci) (zwraca tablice)</p>
<p>[GET] http://machala.net.pl/mup-project/api/public/eksponaty/{id} (przyjmuje id eksponatu) (zwraca obiekt)</p>
<p>[POST] http://machala.net.pl/mup-project/api/public/eksponaty (przyjmuje tytul, opis, url_grafika) (dodaje eksponat do bazy danych i zwraca informacje o powodzeniu lub bledzie w formacie JSON)</p>
<p>[PUT] http://machala.net.pl/mup-project/api/public/eksponaty/{id} (przyjmuje id eksponatu, tytul, opis, url_grafika) (aktualizuje eksponat w bazie danych i zwraca informacje o powodzeniu lub bledzie w formacie JSON)</p>
<p>[DELETE] http://machala.net.pl/mup-project/api/public/eksponaty/{id} (przyjmuje id eksponatu) (kasuje eksponat z bazy danych i zwraca informacje o powodzeniu lub bledzie w formacie JSON)</p>

<h1>Repozytorium na BitBucket.org</h1>
<a href="https://bitbucket.org/hubertmachala/mup-inz-proj-zespolowy/src/develop/">https://bitbucket.org/hubertmachala/mup-inz-proj-zespolowy/src/develop/</a>