<?php
if(isset($_POST['wyslij'])) {
    $tytul = $_POST['tytul'];
    $opis = $_POST['opis'];
    $url_grafika = "";

    if($_FILES["grafika"]["name"] != "") {
        $target_dir = "../images/uploads/";
        $target_file = $target_dir . basename($_FILES["grafika"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        
        
        $check = getimagesize($_FILES["grafika"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            wypisz_blad("File is not an image.");
            $uploadOk = 0;
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            wypisz_blad("Sorry, file already exists.");
            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["grafika"]["size"] > 500000) {
            wypisz_blad("Sorry, your file is too large.");
            $uploadOk = 0;
        }
        
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            wypisz_blad("Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            wypisz_blad("Sorry, your file was not uploaded.");
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["grafika"]["tmp_name"], $target_file)) {
                $url_grafika = $site."/images/uploads/".$_FILES["grafika"]["name"];
                wypisz_sukces("The file ". htmlspecialchars( basename( $_FILES["grafika"]["name"])). " has been uploaded.");
            } else {
                wypisz_blad("Sorry, there was an error uploading your file.");
            }
        }
    }

    $data = array('tytul' => $tytul, 'opis' => $opis, 'url_grafika' => $url_grafika);
        
    $context = stream_context_create(array(
        'http' => array(
        'ignore_errors' => true,
        'method' => "PUT",
        'header'=>  "Content-Type: application/json\r\n" .
        "Accept: application/json\r\n".
        "token: ".$_SESSION['token-mup-projekt']."\r\n",
        'content' => json_encode($data)
        )
    ));

    $url = $API.'/eksponaty/'.$_GET['eksponat'];
    $content = file_get_contents($url, false, $context);
    $result = json_decode($content, true);

    if(isset($result["error"])) {
        wypisz_blad($result["error"]);
    } else {
        wypisz_sukces("Pomyslnie edytowano eksponat");
    }
}


$json = file_get_contents($API.'/eksponaty/'.$_GET['eksponat']);
$eksponat = json_decode($json, true);
?>
<form method="POST" action="" enctype="multipart/form-data">
<div class="form-group">
    <label for="exampleFormControlInput1">Tytuł eksponatu</label>
    <input type="text" class="form-control" name="tytul" value="<?php echo $eksponat['tytul']; ?>" id="exampleFormControlInput1">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Opis eksponatu</label>
    <textarea class="form-control" name="opis" id="exampleFormControlTextarea1" rows="3"><?php echo $eksponat['opis']; ?></textarea>
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Wybierz zdjęcie eksponatu</label>
    <input type="file" class="form-control-file" name="grafika" id="exampleFormControlFile1">
    <p>Jeśli nie dodasz tutaj zdjęcia, to aktualnie przypisane do eksponatu nie zostanie zmienione</p>
  </div>
  <button type="submit" name="wyslij" class="btn btn-primary mb-2">Edytuj eksponat</button>
</form>
<br /><br />
<?php echo $eksponat['url_grafika']; ?><br />
<img src="<?php echo $eksponat['url_grafika']; ?>" alt="" style="max-width: 200px; width: 100px; height: auto; margin-top: 50px;" />