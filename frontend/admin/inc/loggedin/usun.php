<?php
if($_GET['eksponat']) {

    $context = stream_context_create(array(
      'http' => array(
        'ignore_errors' => true,
        'method' => "DELETE",
        'header'=>  "Content-Type: application/json\r\n" .
        "Accept: application/json\r\n".
        "token: ".$_SESSION['token-mup-projekt']."\r\n",
      )
  ));

$url = $API.'/eksponaty/'.$_GET['eksponat'];
$content = file_get_contents($url, false, $context);
$result = json_decode($content, true);
header('Location: '.$site.'/admin/index.php?site=lista');

}