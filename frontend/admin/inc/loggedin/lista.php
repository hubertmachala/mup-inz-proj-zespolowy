<?php
$json = file_get_contents($API.'/eksponaty');
$eksponaty = json_decode($json);
?>

<table class="table">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Tytuł eksponatu</th>
      <th scope="col">Edytuj</th>
      <th scope="col">Usuń</th>
    </tr>
  </thead>
  <tbody>
  <?php
for($i = count($eksponaty)-1; $i >= 0; $i--) {
    ?>
    <tr>
      <th scope="row"><?php echo $eksponaty[$i]->id; ?></th>
      <td><?php echo $eksponaty[$i]->tytul; ?></td>
      <td><a href="<?php echo $site; ?>/admin/index.php?site=edytuj&eksponat=<?php echo $eksponaty[$i]->id; ?>">[ Edytuj ]</a></td>
      <td><a href="<?php echo $site; ?>/admin/index.php?site=usun&eksponat=<?php echo $eksponaty[$i]->id; ?>">[ Usuń ]</a></td>
    </tr>
    <?php } ?>
</tbody>
</table>