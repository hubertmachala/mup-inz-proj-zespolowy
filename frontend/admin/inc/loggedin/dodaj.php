<?php
if(isset($_POST['wyslij'])) {
$tytul = $_POST['tytul'];
$opis = $_POST['opis'];
//$grafika = $_POST['grafika'];


$target_dir = "../images/uploads/";
$target_file = $target_dir . basename($_FILES["grafika"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


$check = getimagesize($_FILES["grafika"]["tmp_name"]);
if($check !== false) {
  $uploadOk = 1;
} else {
  wypisz_blad("File is not an image.");
  $uploadOk = 0;
}


// Check if file already exists
if (file_exists($target_file)) {
wypisz_blad("Sorry, file already exists.");
$uploadOk = 0;
}

// Check file size
if ($_FILES["grafika"]["size"] > 500000) {
wypisz_blad("Sorry, your file is too large.");
$uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
wypisz_blad("Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
$uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
wypisz_blad("Sorry, your file was not uploaded.");
// if everything is ok, try to upload file
} else {
if (move_uploaded_file($_FILES["grafika"]["tmp_name"], $target_file)) {
  wypisz_sukces("The file ". htmlspecialchars( basename( $_FILES["grafika"]["name"])). " has been uploaded.");



$url_grafika = $site."/images/uploads/".$_FILES["grafika"]["name"];
$data = array('tytul' => $tytul, 'opis' => $opis, 'url_grafika' => $url_grafika);

  $context = stream_context_create(array(
    'http' => array(
      'ignore_errors' => true,
      'method' => "POST",
      'header'=>  "Content-Type: application/json\r\n" .
      "Accept: application/json\r\n".
      "token: ".$_SESSION['token-mup-projekt']."\r\n",
      'content' => json_encode($data)
    )
));

$url = $API.'/eksponaty';
$content = file_get_contents($url, false, $context);
$result = json_decode($content, true);


if(isset($result["error"])) {
  wypisz_blad($result["error"]);
} else if(isset($result["tytul"])) {
    wypisz_blad($result["tytul"]);
} else if(isset($result["opis"])) {
    wypisz_blad($result["opis"]);
} else {
  wypisz_sukces("Pomyslnie dodano eksponat");
  // header( 'refresh:5;url='.$site.'/admin/index.php?site=lista' );
}




} else {
  wypisz_blad("Sorry, there was an error uploading your file.");
}
}

}

?>

<form method="POST" action="" enctype="multipart/form-data">
<div class="form-group">
    <label for="exampleFormControlInput1">Tytuł eksponatu</label>
    <input type="text" class="form-control" name="tytul" id="exampleFormControlInput1">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Opis eksponatu</label>
    <textarea class="form-control" name="opis" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Wybierz zdjęcie eksponatu</label>
    <input type="file" class="form-control-file" name="grafika" id="exampleFormControlFile1">
  </div>
  <button type="submit" name="wyslij" class="btn btn-primary mb-2">Dodaj eksponat</button>
</form>