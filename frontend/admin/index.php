<?php
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
session_start();
include_once("../config.php");
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin - Inzynierski Projekt</title>
    <meta name="description" content="Admin - Grupowy projekt inynierski - 4 rok - Mazowiecka Uczelnia Publiczna w Płocku" />
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    
<?php
if($_SESSION['token-mup-projekt']) {
?>

  <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="<?php echo $site; ?>/admin/index.php">Panel Admina - Grupowy Projekt Inzynierski</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="<?php echo $site; ?>/admin/index.php?site=logout">Wyloguj</a>
        </li>
      </ul>
    </nav>


    <div class="container-fluid">
      <div class="row">
        <nav class="col-4 col-md-3 col-lg-2 d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link <?php if($_GET['site'] == "home" || !$_GET['site']) { echo "active"; } ?>" href="<?php echo $site; ?>/admin/index.php?site=home">
                  Strona główna
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link <?php if($_GET['site'] == "czlonkowie") { echo "active"; } ?>" href="<?php echo $site; ?>/admin/index.php?site=czlonkowie">
                  Członkowie grupy
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link <?php if($_GET['site'] == "dokumentacja") { echo "active"; } ?>" href="<?php echo $site; ?>/admin/index.php?site=dokumentacja">
                  Dokumentacja API / linki
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo $site; ?>">
                  Przejdź do aplikacji
                </a>
              </li>
            </ul>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Eksponaty</span>
              <a class="d-flex align-items-center text-muted" href="#">
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link <?php if($_GET['site'] == "lista") { echo "active"; } ?>" href="<?php echo $site; ?>/admin/index.php?site=lista">
                  Lista eksponatów
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link <?php if($_GET['site'] == "dodaj") { echo "active"; } ?>" href="<?php echo $site; ?>/admin/index.php?site=dodaj">
                  Dodaj eksponat
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-8 offset-4 col-md-9 offset-md-3 col-lg-10 offset-lg-2 px-8" style="padding-left: 25px; padding-top: 65px; padding-bottom: 25px;">


<?php
if($_GET['site']) {
  include('inc/loggedin/'.$_GET['site'].'.php');
} else {
  include('inc/loggedin/home.php');
}
?>

        </main>
</div>
</div>

<?php
} else {
  include('inc/loggedout/login.php');
}
?>

<script src="../js/bootstrap.bundle.min.js"></script>
</body>
</html>