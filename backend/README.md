# Instalacja
1. Tworzymy klona repozytorium z GitHub
2. Wykonujemy polecenie composer install - katalog vendor jest niezbędny do poprawnego działania
3. Tworzymy kopie pliku .env.example, zmieniamy nazwę na .env i zmieniamy w nim:
4. Wykonujemy polecenie php artisan migrate w celu stworzenia tabel w bazie danych

LUB

Importujemy plik database.sql do naszej bazy danych (tej, której dane podaliśmy w pliku .env) 3. Wgrywamy wszystkie pliki do katalogu public_html na serwerze www 4. W katalogu public_html na serwerze tworzymy plik .htaccess o zawartości:

`
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteRule ^(.*)$ public/$1 [L]
</IfModule>
`

# Dev

Serwer lokalny `php -S localhost:8000 -t public`

Odświeżenie paczek w katalogu `vendor` - `composer install`

Tworzenie migracji tabel `php artisan make:migration create_users_table`

Tworzenie modelu `php artisan make:model User`

Tworzenie kontrolera `php artisan make:controller UserController`

Wykonanie migracji `php artisan migrate`

Usunięcie wszystkich tabel i utworzenie od nowa `php artisan migrate:fresh`


# Lumen PHP Framework

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
