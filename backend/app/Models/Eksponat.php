<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Eksponat extends Model
{
    protected $table = 'eksponaty';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tytul','opis','url_grafika'
    ];
}