<?php

namespace App\Http\Controllers;

use App\Models\Eksponat;
use Illuminate\Http\Request;

class EksponatyController extends Controller
{
    /**
     * The request instance.
     * @var Request
     */
    private $request;

    /**
     * Create a new controller instance.
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        return response()->json(Eksponat::all());
    }

    public function create(Request $request)
    {
        $this->validate($this->request, [
            'tytul' => 'required',
            'opis' => 'required',
            'url_grafika' => 'required'
        ]);

        try {
            $eksponat = new Eksponat;
            $eksponat->tytul = $request->tytul;
            $eksponat->opis = $request->opis;
            $eksponat->url_grafika = $request->url_grafika;
            $eksponat->save();

            return response()->json([
                'success' => 'Eksponat created successfully',
                'eksponat' => $eksponat
            ], 201);

        } catch (\Throwable $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function show($id)
    {
        $eksponat = Eksponat::find($id);

        if(!$eksponat) {
            return response()->json([
                'error' => 'Eksponat does not exist.'
            ], 404);
        }

        return response()->json($eksponat);
    }

    public function update(Request $request, $id)
    {
        $eksponat = Eksponat::find($id);

        if(!$eksponat) {
            return response()->json([
                'error' => 'Eksponat does not exist.'
            ], 404);
        }

        try {
            $eksponat->tytul = $request->input('tytul') ?: $eksponat->tytul;
            $eksponat->opis = $request->input('opis') ?: $eksponat->opis;
            $eksponat->url_grafika = $request->input('url_grafika') ?: $eksponat->url_grafika;
            $eksponat->save();

            return response()->json([
                'success' => 'Eksponat updated successfully',
                'eksponat' => $eksponat
            ], 201);
        } catch (\Throwable $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function destroy($id)
    {
        $eksponat = Eksponat::find($id);

        if(!$eksponat) {
            return response()->json([
                'error' => 'Eksponat does not exist.'
            ], 404);
        }

        try {
            $eksponat->delete();

            return response()->json([
                'success' => 'Eksponat removed successfully',
                'eksponat' => $eksponat
            ], 202);
        } catch (\Throwable $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }
}